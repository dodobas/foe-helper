import json
import itertools
import pickle
import glob

from players import Player


def print_current_data():
    all_players = load_players()

    last_data_file = sorted(glob.glob('data/**/*.pickle', recursive=True), reverse=True)[0]

    with open(last_data_file, 'rb') as krastavac:
        all_data = pickle.load(krastavac)

    cur_players = []

    for row in itertools.chain(*all_data):
        player = Player.from_row(row)

        if player.guild_id == 6509:
            print(player)
            cur_players.append(player)

    export_data =  []

    for player in sorted(cur_players, reverse=True):
        if player.player_id in all_players:
            player_metadata = all_players[player.player_id]
            player_metadata['points'] = player.points
            player_metadata['battles'] = player.battles

            export_data.append(player_metadata)

    data_for_export = json.dumps(export_data)
    with open('./public/static/players.js', 'wt') as fp:
        template = f'var all_players = {data_for_export};'
        fp.write(template)


def load_players():
    with open('config/players.json', 'rb') as fp:
        return json.load(fp)


if __name__ == '__main__':
    print_current_data()
