
class Player:

    def __init__(self, player_id, player_name, guild_id, guild_name, points, battles):
        self.player_id = player_id
        self.player_name = player_name
        self.guild_id = guild_id
        self.guild_name = guild_name
        self.points = points
        self.battles = battles

    @classmethod
    def from_row(cls, row):
        return cls(*cls.parse_row(row))

    @staticmethod
    def parse_row(row):
        player_name = row[2]
        guild_name = row[3]
        points = int(''.join(row[4].split(',')))
        battles = int(''.join(row[5].split(',')))
        player_id = row[7]
        guild_id = int(row[6])

        return player_id, player_name, guild_id, guild_name, points, battles

    def __str__(self):
        return f'{self.player_id} {self.player_name} {self.points} {self.battles}'

    def __lt__(self, other):
        return self.points < other.points
