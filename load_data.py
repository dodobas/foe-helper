import json
import time
import requests
import pickle
import os


def check_if_updated():
    HEADERS = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Host': 'www.forge-db.com',
        'Pragma': 'no-cache',
        'Referer': 'http://www.forge-db.com/en/',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'
    }

    URL = 'http://www.forge-db.com/en/en14/?server=en14&world=Odhrorvar'

    res = requests.get(URL, HEADERS)

    start_index = res.text.find('Last Updated:') + 14
    end_index = res.text.find('<br>', start_index)

    timestamp_str = res.text[start_index:end_index]

    if timestamp_str[-5:] == '(EST)':
        timestamp = time.strptime(f'{timestamp_str[:-5]} -0500', '%B %d, %Y %H:%M %z')
    elif timestamp_str[-5:] == '(EDT)':
        timestamp = time.strptime(f'{timestamp_str[:-5]} -0400', '%B %d, %Y %H:%M %z')
    else:
        raise ValueError(f'Unknown timezone: {timestamp_str[-5:]}')

    return timestamp


def fetch_remote_data():
    HEADERS = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'en-US,en;q=0.5',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Host': 'www.forge-db.com',
        'Pragma': 'no-cache',
        'Referer': 'http://www.forge-db.com/en/en14/players/?server=en14&world=Odhrorvar',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0',
        'X-Requested-With': 'XMLHttpRequest'
    }

    start = 0
    length = 5000
    total = 1
    page = 1

    all_data = []

    while start < total:
        ts = int(time.time())

        URL = f'http://www.forge-db.com/en/en14/getPlayers.php?draw={page}&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=false&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=3&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=4&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=true&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=5&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=6&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=7&columns%5B7%5D%5Bname%5D=&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc&start={start}&length={length}&search%5Bvalue%5D=&search%5Bregex%5D=false&_={ts}'

        res = requests.get(URL, HEADERS)

        data = res.json()

        all_data.append(data.get('data'))
        total = data.get('recordsTotal')

        start += length

        page += 1

    return all_data


def generate_data_filename(timestamp: time.struct_time):
    filename = os.path.join(
        'data', f'{timestamp.tm_year}', f'{timestamp.tm_mon:02d}',
        f'data_{time.strftime("%Y%m%d_%H%M", timestamp)}.pickle'
    )

    return filename


def load_players():
    with open('config/players.json', 'rb') as fp:
        return json.load(fp)


if __name__ == '__main__':
    # read latest timestamp
    with open('last_update.pickle', 'rb') as fp:
        last_update = pickle.load(fp)

    print('checking if data was updated...')
    timestamp = check_if_updated()
    print('done')

    if timestamp != last_update:
        # write new timestamp
        with open('last_update.pickle', 'wb') as fp:
            pickle.dump(timestamp, fp)

        print('fetching new data...')
        new_data = fetch_remote_data()
        print('done')

        # write new data
        with open(generate_data_filename(timestamp), 'wb') as fp:
            pickle.dump(new_data, fp)
    else:
        print('No new data!')
